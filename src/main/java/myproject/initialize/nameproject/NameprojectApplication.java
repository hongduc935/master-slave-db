package myproject.initialize.nameproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NameprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(NameprojectApplication.class, args);
	}

}
